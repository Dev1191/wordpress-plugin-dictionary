<?php
/**
 * @package DictionaryEandS
 */
/*
Plugin Name: DictionaryEandS - English and Somali
Plugin URI: https://ldsengineers.com/
Description: This is custom made dictonaryEandS for the manageing the english and Somali
Author: Dave Raj
Author URI: https://ldsengineers.com/
License: GPLv2 or later
Text Domain: DictonaryEandS
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2010-2018 Automattic, Inc.
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}
define( 'DICTIONARY_VERSION', '1.0.0' );
define( 'DICTIONARY__MINIMUM_WP_VERSION', '1.0' );
define( 'DICTIONARY_PLUGIN_DIR',plugin_dir_path(__FILE__));
define( 'DICTIONARY_IMAGE_PATH',plugin_dir_url(__FILE__));

//echo get_stylesheet_directory_uri();

require_once(DICTIONARY_PLUGIN_DIR.'includes/init.php');


require_once(DICTIONARY_PLUGIN_DIR.'includes/admin_init.php');

require_once(DICTIONARY_PLUGIN_DIR.'process/save_post.php');

add_action('init', 'dictionary_register');
add_action('admin_init','dictionary_admin_meta_box');
add_action( 'admin_enqueue_scripts', 'load_dictionary_css');
add_action('save_post_dictionary','dic_save_post_admin', 10, 3);
// add_filter('manage_edit-dictionary_columns','add_columns');
// add_filter('manage_edit-dictionary_sortable_columns','short_columns');


function load_dictionary_css(){
	wp_register_style('boot-dictionary.css', plugins_url('dictionaryEandS/css/style.css'),__FILE__);
	wp_enqueue_style('boot-dictionary.css');
}
