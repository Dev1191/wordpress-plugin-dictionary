<?php 


function  dic_save_post_admin($post_id,$post,$update){

	if(!$update){
		return;
	}

	$dictionary_data = array();
	$dictionary_data['expleone'] = sanitize_textarea_field($_POST['dic_expone'],true);
	$dictionary_data['expletwo'] = sanitize_textarea_field($_POST['dic_exptwo'],true);
	$dictionary_data['explethree'] = sanitize_textarea_field($_POST['dic_expthree'],true);
	$dictionary_data['explefour'] = sanitize_textarea_field($_POST['dic_expfour'],true);

	$data = update_post_meta($post_id,'dictionary_data',$dictionary_data);

	print_r($data);
}