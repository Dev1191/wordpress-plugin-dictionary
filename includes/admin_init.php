<?php 


function  dictionary_admin_meta_box(){
add_action('add_meta_boxes_dictionary','reg_create_metaboxes');

}


function  reg_create_metaboxes(){
	add_meta_box(
		'meta_dictionary-id',
		__( 'Example Options','dictionary'),
		'r_dictionary_options_mb',
		'dictionary',
		'normal',
		'high'
		);
}

function  r_dictionary_options_mb($post){

	$dictionary_data = get_post_meta($post->ID,'dictionary_data',true);
	
	if(!$dictionary_data){
		$dictionary_data = array(
			'expleone' => '',
			'expletwo' => '',
			'explethree' => '',
			'explefour' => '',
		);
	}
?>
	<div class="form-group">
		<label for=""><b>Example #1</b></label>
		<textarea class="form-control" name="dic_expone" rows="5" cols="100"><?php echo $dictionary_data['expleone'] ?></textarea>
	</div>
	<div class="form-group">
		<label for=""><b>Example #2</b></label>
			<textarea class="form-control" name="dic_exptwo" rows="5" cols="100"><?php echo $dictionary_data['expletwo'] ?></textarea>
	</div>
		<div class="form-group">
		<label for=""><b>Example #3</b></label>
			<textarea class="form-control" name="dic_expthree" rows="5" cols="100"><?php echo $dictionary_data['explethree'] ?></textarea>
	</div>
		<div class="form-group">
		<label for=""><b>Example #4</b></label>
			<textarea class="form-control" name="dic_expfour" rows="5" cols="100"><?php echo $dictionary_data['explefour'] ?></textarea>
	</div>

<?php
}





