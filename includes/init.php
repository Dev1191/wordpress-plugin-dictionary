<?php 
add_action( 'restrict_manage_posts', 'dictionary_add_taxonomy_filters' );
add_action( 'admin_head' , 'dictionary_remove_editor_upload' );
// add_action('manage_custom_post_columns','custom_post_columns');


 
function dictionary_register() {
 
	$labels = array(
		'name' => _x('Dictionary', 'post type general name'),
		'singular_name' => _x('Dictionary Item', 'post type singular name'),
		'add_new' => _x('Add New Enter', 'Title'),
		'add_new_item' => __('Add New Title'),
		'edit_item' => __('Edit Dictionary'),
		'new_item' => __('New dictionary '),
		'view_item' => __('View dictionary'),
		'search_items' => __('Search dictionary'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);
 
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => DICTIONARY_IMAGE_PATH . 'images/book-logo.png',
		'rewrite' => array( 'slug' => 'dictionary' ), 
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => 20,
		'supports' => array('title','editor'),
	  ); 
 
	register_post_type( 'dictionary' , $args );

    create_dictionary_tag();
    create_dictionary_taxonomies();

}

/**
* create the category word type 
**/
function create_dictionary_taxonomies(){
		$taxlabels = array(
		'name'              => _x( 'Word Type', 'Word type', 'textdomain' ),
		'singular_name'     => _x( 'Wordtype', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => _x( 'Search Word Type', 'textdomain' ),
		'all_items'         => __( 'All Word Types', 'textdomain' ),
		'parent_item'       => __( 'Parent Word Type', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Word Type:', 'textdomain' ),
		'edit_item'         => _x( 'Edit Word Type', 'textdomain' ),
		'update_item'       => _x( 'Update Word Type', 'textdomain' ),
		'add_new_item'      => _x( 'Add Word Type', 'textdomain' ),
		'new_item_name'     => _x( 'New Word Type Name', 'textdomain' ),
		'menu_name'         => __( 'Word Type', 'textdomain' ),
	);


		$taxargs = array(
        'hierarchical' => true, 
        'labels' => $taxlabels , 
        'singular_label' => 'Word Type', 
        'rewrite' => array( 'slug' => 'wordtype'),
        'show_admin_column' => true,
        'query_var'             => true,
        );


   register_taxonomy( 'wordtype', array('dictionary'), $taxargs);


}

/**
* create the category lang_tag
**/
function create_dictionary_tag(){
		$taxlabels = array(
		'name'              => _x( 'Lang Tag in English', 'Lang Tag', 'textdomain' ),
		'singular_name'     => _x( 'Lang_tag', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => _x( 'Search Lang Tag', 'textdomain' ),
		'all_items'         => __( 'All Lang Tags', 'textdomain' ),
		'parent_item'       => __( 'Parent Lang Tag', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Lang Tag:', 'textdomain' ),
		'menu_name'         => __( 'New Lang Tag', 'textdomain' ),
	);


		$taxargs = array(
        'hierarchical' => true, 
        'label' => 'Lang Tag' , 
        'singular_label' => 'Language tag', 
        'rewrite' => array( 'slug' => 'lang_tag'),
         'show_admin_column' => true,
         'query_var'          => true,
         'short' => true
        );


   register_taxonomy( 'lang_tag', array('dictionary'), $taxargs);


}


/**
* add filter by wordtype and lang tag
**/
function dictionary_add_taxonomy_filters() {
	global $typenow;
 
	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array('wordtype','lang_tag');
 
	// must set this to the post type you want the filter(s) displayed on
	if( $typenow == 'dictionary' ){
 
		foreach ($taxonomies as $tax_slug) {
			$tax_obj = get_taxonomy($tax_slug);


			$tax_name = $tax_obj->labels->name;
		
			$terms = get_terms($tax_slug);


			if(count($terms) > 0) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Show All $tax_name</option>";

				foreach ($terms as $term) { 
					print_r($term);
					echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
	}
}


/**
*  hide the media icon 
*/
function  dictionary_remove_editor_upload(){
    global $post;
    if(isset($post) && $post->post_type ==  'dictionary'){
        remove_action( 'media_buttons', 'media_buttons' );
    }
}



// /**
// * add custom columns
// * @param $columns array
// */
// function add_columns($columns){
// 	$columns['dictionary_word_type'] = 'Word Type';
// 	$columns['dictionary_lang_type'] = 'Lang Type';

// 	unset($columns['comments']);
// 	return $columns;
	
// }


// /**
// * managing custom columns
// * @param $columns string
// */
// function custom_post_columns($columns){
// 	if('dictionary_word_type' == $columns){
// 		$wordtype = esc_html(get_post_meta(ge_the_ID()),'wordtype',true);
// 		echo $wordtype;

// 	}else if('dictionary_lang_type' ==  $columns){
// 		$langtag = esc_html(get_post_meta(get_the_ID()),'lang_tag',true);
// 		echo $langtag;
// 	}
// }

// /**
// * sortable the custom columns 
// */
// function short_columns($columns){
// 	$columns['short_word_type'] = 'short_word_type';
// 	$columns['short_lang_tag'] = 'short_lang_tag';
// 	return $columns;
// }



// add_filter( 'request', 'column_ordering' );
 
// add_filter( 'request', 'column_orderby' );
 
// function column_orderby ( $vars ) {
//     if ( !is_admin() )
//         return $vars;
//     if ( isset( $vars['orderby'] ) && 'short_word_type' == $vars['orderby'] ) {
//         $vars = array_merge( $vars, array( 'meta_key' => 'wordtype', 'orderby' => 'meta_value' ) );
//     }
//     elseif ( isset( $vars['orderby'] ) && 'short_lang_tag' == $vars['orderby'] ) {
//         $vars = array_merge( $vars, array( 'meta_key' => 'lang_tag', 'orderby' => 'meta_value_num' ) );
//     }
//     return $vars;
// }